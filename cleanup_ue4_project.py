# pyinstaller -F --onefile cleanup_ue4_project.py
import os
import shutil
from msvcrt import getch


def remove_directories(directories):
    for directory in directories:
        try:
            shutil.rmtree(directory)
            print(f"REMOVE: " + directory)
        except Exception as ex:
            # print(ex)
            pass


if __name__ == "__main__":
    current_dir_path = os.path.abspath(os.curdir)
    plugins_dir_parh = current_dir_path + "\\" + "Plugins"
    saved_dir_path = current_dir_path + "\\" + "Saved"

    ide_files = [current_dir_path + "\\" + name for name in os.listdir(current_dir_path)
                   if name.__contains__('.sln') or name.__contains__('.code-workspace') or name.__contains__('.vscode')]
    for remove in ide_files:
        try:
            os.remove(remove)
        except Exception as ex:
            # print(ex)
            pass

    directories_to_remove = [current_dir_path + "\\" + ".idea",
                             current_dir_path + "\\" + ".vs",
                             current_dir_path + "\\" + "Binaries",
                             current_dir_path + "\\" + "Intermediate",
                             current_dir_path + "\\" + "Saved",
                             current_dir_path + "\\" + "Build"]
    remove_directories(directories_to_remove)

    try:
        remove_saved = [saved_dir_path + "\\" + save for save in os.listdir(saved_dir_path) if
                        os.path.isdir(os.path.join(saved_dir_path, save))]
        remove_directories(remove_saved)
    except Exception as ex:
        # print(ex)
        pass

    try:
        plugins_binares_directories = [plugins_dir_parh + "\\" + plugin + "\\" + "Binaries"
                                  for plugin in os.listdir(plugins_dir_parh)
                                  if os.path.isdir(os.path.join(plugins_dir_parh, plugin))]
        remove_directories(plugins_binares_directories)
    except Exception as ex:
        # print(ex)
        pass

    try:
        plugins_intermediate_directories = [plugins_dir_parh + "\\" + plugin + "\\" + "Intermediate"
                                       for plugin in os.listdir(plugins_dir_parh)
                                       if os.path.isdir(os.path.join(plugins_dir_parh, plugin))]
        remove_directories(plugins_intermediate_directories)
    except Exception as ex:
        # print(ex)
        pass

    print("Press Any Key To Exit...")
    junk = getch()

