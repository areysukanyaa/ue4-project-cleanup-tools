**UE4 PROJECT CLEANUP TOOLS**

_ShaderCompilerFixer.bat_ 

Bat file to speed up compilation of shaders, that sets the ShaderCompileWorker process high priority.


_cleanup_ue4_project.py_

Script who remove files generated ue4 and visual studio files, that might be causing the compilation error. 
Script remove .sln, .code-workspace, .vscode files and remove .idea, .vs Saved, Binaries, Intermediate directories in project and Binaries, Intermediate directories in project plugins.


**REQUIREMENTS**

Python 3.6 and higher.


**INSTALLATION**

If you use pycharm, than build cleanup_ue4_project.py to .exe with command "pyinstaller -F --onefile cleanup_ue4_project.py".
Then copy %project_name%/dist/cleanup_ue4_project.exe and paste in you UE4 project root directory.


**QUICK START**

Run the ShaderCompilerFixer.bat file when building shaders.
Run cleanup_ue4_project.exe from the project root to clean it up.
